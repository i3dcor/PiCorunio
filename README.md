PiCorunio

El ordenador Pi más pequeño del mundo

Consiste en:

Ordenador Raspberry Pi 3

Pantalla TFT táctil de 3.5"

Batería de litio 3.7v 4000mah

Cargador de batería

Conversor de alimentación de 3.7v a 5v

Interruptor de alimentación

Tarjeta microSD con sistema operativo linux

Todo en una reducida caja diseñada en 3d y fabricada con impresora 3d

Se recomienda usar un teclado inalámbrico de tamaño reducido