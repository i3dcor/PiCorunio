//TFT screen pimoroni hyperpixel

//TODO

//tuerca hueco microsd y hueco tornillo tapa tft
//redondear esquinas
//orificios led cargador

$fn=36;
tolerance=0.4;
x=0;
y=1;
z=2;
font1 = "Liberation Sans"; // here you can select other font type
content = "mikoko";

pi3_wall=1.6;
pi3_pcb=[85+tolerance*2,56+tolerance*2,1.2];
pi3_pcb_position=[tolerance,tolerance,2];
pi3_component_position_z=pi3_pcb[z]+pi3_pcb_position[z];

pi3_screw_support_diameter=6+tolerance*2;
pi3_screw_diameter=2.5;
pi3_pcb_hole1=[3.5,3.5+tolerance,0];
pi3_pcb_hole2=[3.5,3.5+49+tolerance*2,0];
pi3_pcb_hole3=[3.5+58,3.5+tolerance,0];
pi3_pcb_hole4=[3.5+58,3.5+49+tolerance*2,0];

tft=[88+tolerance,52+tolerance*2,3+tolerance];
tft_pcb=[89,57,2];
tft_pcb_position=[-4,0,pi3_component_position_z+18];
tft_position=[pi3_pcb[x]-tft[x],pi3_pcb[y]/2-tft[y]/2,tft_pcb_position[z]+tft_pcb[z]];

micro_usb=[9+tolerance,6,3+tolerance];
micro_usb_distance2pcb=-0.1;
micro_usb_distance2left=6.4;

hdmi=[17+tolerance*2,10,8+tolerance];
hdmi_distance2pcb=1;
hdmi_distance2left=23;

jack_diameter=7.5+tolerance;
jack_distance2left=53;

ethernet=[21.5,17+tolerance,14.5+tolerance];
ethernet_position=[67,2.5+8-ethernet[y]/2,pi3_component_position_z];

usb2=[17.5,16.5+tolerance,17+tolerance];
usb12_position=[71,2.5+45-usb2[y]/2,pi3_component_position_z];
usb34_position=[71,2.5+27-usb2[y]/2,pi3_component_position_z];

battery=[65,53,7];
battery2=[77,40,10];
battery_offset=5;
battery_charger_wall=1;
battery_charger=[17+tolerance,28+tolerance*2,1.5+battery_offset];

battery_converter5v=[15+tolerance,19+tolerance,5+battery_offset];
battery_converter5v_position=[3-battery_charger[x],pi3_pcb[y]-battery_converter5v[y],0];

battery_connector=[10.5+tolerance,14+tolerance,2.5+battery_offset];
battery_connector_position=[battery_charger[x]/2-battery_connector[x]/2,battery_charger[y],1.5];
battery_box=[battery_charger[x], pi3_pcb[y]+battery_charger_wall*2,hdmi[z]-battery_charger_wall];

switch_pcb=[14,6.5,9.5];
switch_pcb_position=[-battery_charger[x]+pi3_wall+2,pi3_pcb[y]-switch_pcb[x]/2-3,5.5];
battery_charger_position=[-battery_charger[x],0,pi3_component_position_z+2];

micro_sd=[6+battery_box[x],12+tolerance*2,2.5+tolerance+pi3_wall+pi3_pcb[z]];
//aligned to middle left of pcb
micro_sd_position=[-pi3_wall-battery_box[x],pi3_pcb[y]/2-micro_sd[y]/2,-pi3_wall];

//box
minkowski_box=[pi3_pcb[x]-pi3_screw_support_diameter+tolerance,pi3_pcb[y] - pi3_screw_support_diameter+tolerance,tft_pcb_position[z]+tft_pcb[z]];
// minkowski_box=[pi3_pcb[0]-pi3_screw_support_diameter,pi3_pcb[1] - pi3_screw_support_diameter,jack_diameter+pi3_component_position_z];
difference(){
union(){
	translate([4,-7.5,0])
	translate(switch_pcb_position)
		rotate([0,0,90])
			scale([1.2,1.2,1.9])
			cube(switch_pcb);
	translate([-battery_box[x],0,0])
	translate([-pi3_wall,-pi3_wall,-pi3_wall])
	translate([pi3_screw_support_diameter/2,pi3_screw_support_diameter/2,0])
		minkowski(){
			cube([minkowski_box[x]+battery_box[x]+pi3_wall*2,minkowski_box[y]+pi3_wall*2,minkowski_box[z]]);
			cylinder(d=pi3_screw_support_diameter,h=tft[z]);
			//sphere(d=pi3_screw_support_diameter);

		}
}
//sustract pcb 
translate([pi3_screw_support_diameter/2-tolerance,pi3_screw_support_diameter/2,0])
minkowski(){
		cube(minkowski_box);
		  cylinder(d=pi3_screw_support_diameter,h=0.01);
	}
//microsd 
translate(micro_sd_position)
	cube(micro_sd);

//microusb hide
translate([micro_usb_distance2left, -1,pi3_component_position_z])
	cube([micro_usb[x],micro_sd[y],micro_sd[z]*2]);
	
//hdmi
translate([hdmi_distance2left, -pi3_wall,pi3_component_position_z])
	cube(hdmi);
//jack
translate([jack_distance2left, -pi3_wall,pi3_component_position_z+jack_diameter/2])
	rotate([-90]) cylinder(d=jack_diameter,h=jack_diameter);
//ethernet
translate(ethernet_position)
	cube(ethernet);
//usb12
translate(usb12_position)
	cube(usb2);
//usb34
translate(usb34_position)
	cube(usb2);

translate(battery_charger_position){
	cube(battery_charger);
	//battery connector hole
	translate(battery_connector_position)
		cube(battery_connector);
	translate([battery_charger[x]/2-micro_usb[x]/2,-pi3_wall,1.5])
		cube(micro_usb);

	translate([0,0,battery_charger[z]])
		cube([battery_box[x],battery_box[y]-battery_charger_wall*2,tft_position[z]- battery_charger_position[z]-battery_charger[z]]);
}
translate(battery_converter5v_position)
	cube(battery_converter5v);
//TODO switch hole
translate(switch_pcb_position)
	import("switch.stl");

translate(tft_pcb_position)
	cube(tft_pcb);
translate(tft_position)
	cube(tft);

// translate(tft_position) translate([-pi3_wall,0,pi3_wall/2])
	// rotate([0,0,90])
		// linear_extrude(height = pi3_wall)
			// text(content, font = font1, size = 12, direction = "ltr", spacing = 1 );

translate([pi3_pcb[x]-15,pi3_pcb[y]+pi3_wall/2,8])
	rotate([90,0,180])
		linear_extrude(height = pi3_wall)
			text(content, font = font1, size = 14, direction = "ltr", spacing = 1.2 );
 //screw holes
 translate([0,0,-pi3_wall]) 
 translate(pi3_pcb_hole3) 
	cylinder(d=pi3_screw_diameter,h=pi3_pcb_position[z]);
 translate([0,0,-pi3_wall]) 
 translate(pi3_pcb_hole4) 
	cylinder(d=pi3_screw_diameter,h=pi3_pcb_position[z]);
 }

//posts 
 translate(pi3_pcb_hole1) rotate([0,0,180]) screw_hole();
 translate(pi3_pcb_hole2) screw_hole();
 translate(pi3_pcb_hole3) rotate([0,0,180])	screw_hole();
 translate(pi3_pcb_hole4) screw_hole();
 
 //holes top
 translate([0,0,8]) scale([1,1,1])
 translate(pi3_pcb_hole3) rotate([0,0,180]) screw_hole();
 translate([-pi3_screw_diameter,-pi3_screw_diameter-pi3_wall+0.5,4]) 
 translate(pi3_pcb_hole3) cube([pi3_screw_diameter*2,pi3_wall,4]);

 translate([0,0,8]) scale([1,1,1])
 translate(pi3_pcb_hole4) screw_hole();
 translate([-pi3_screw_diameter,pi3_screw_diameter-0.5,4]) 
 translate(pi3_pcb_hole4) cube([pi3_screw_diameter*2,pi3_wall,4]);

//screw holes
module screw_hole(){
difference(){
	union(){
		cylinder(d=pi3_screw_support_diameter,h=pi3_pcb_position[z]);
		translate([-pi3_screw_support_diameter/2,0,0])
			cube([pi3_screw_support_diameter,pi3_screw_support_diameter/2+tolerance*2,pi3_pcb_position[z]]);
	}
	cylinder(d=pi3_screw_diameter,h=pi3_pcb_position[z]);
}
}